ASSUME CS:codesg,DS:datasg
datasg segment
    tmpB DB 'Hello,World!$'
datasg ends
codesg segment
  start:
    mov AX,datasg
    mov DS,AX
    LEA DX,tmpB
    mov AH,09
    int 21H
    mov ax,4C00H
    int 21H
codesg ends
  end start
